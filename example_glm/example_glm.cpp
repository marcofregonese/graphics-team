/******************************************************************
*
* Interaction.c  
*
* Description: This file demonstrates the loading of external 
* triangle meshes provided in OBJ format. In addition, user
* interaction via mouse and keyboard is employed.
*
* The loaded triangle mesh is draw in wireframe mode and rotated
* around a reference axis. The user can control the rotation axis
* via the mouse and start/stop/reset animation via the keyboard.
*
* Computer Graphics Proseminar SS 2018
* 
* Interactive Graphics and Simulation Group
* Institute of Computer Science
* University of Innsbruck
*
*******************************************************************/


/* Standard includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* OpenGL includes */
#include <GL/glew.h>
#include <GL/freeglut.h>

/* glm include */
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/* Local includes */
#include "LoadShader.h"    /* Loading function for shader code */
#include "LoadTexture.h"   /* Loading function for BMP texture */
#include "Matrix.h"        /* Functions for matrix handling */
#include "Vector.h"        /* Functions for vector handling */
#include "OBJParser.h"     /* Loading function for triangle meshes in OBJ format */


/*----------------------------------------------------------------*/
/* Window parameters */
float winWidth = 1000.0f;
float winHeight = 800.0f;

/* Flag for starting/stopping animation */
GLboolean anim = GL_TRUE;

/* Define handles to several vertex buffer objects */
GLuint VBOs[20];

/* Define handle to several color buffer objects */
GLuint CBOs[20];

/* Define handle to several normal buffer object */
GLuint NBOs[20];

/* Define handles to several index buffer objects */
GLuint IBOs[20];

/* Define handle to several uv buffer object */
GLuint UVBOs[20];

/* Indices to vertex attributes; in this case positon only */ 
enum DataID {vPosition = 0, vColor = 1, vNormal = 2, vUV = 3};

/* Strings for loading and storing shader code */
static const char* VertexShaderString;
static const char* FragmentShaderString;

GLuint ShaderProgram;

/* Number of objects in the scene */
int nbObjects = 20;

/* Define matrices */
glm::mat4 ProjectionMatrix;     /* Perspective projection matrix */
glm::mat4 ViewMatrix;           /* Camera view matrix */
glm::mat4 ViewMatrix1;           /* Camera view matrix */

glm::mat4 ModelMatrix[20];       /* array of model matrices */
  
/* Transformation matrices */
glm::mat4 Translate_01_00_00;
glm::mat4 Translate_03_00_00;
glm::mat4 Translate_05_00_00;
glm::mat4 Translate_08_02_00;
glm::mat4 Translate_00_05_40;
glm::mat4 Translate_07_00_00;
glm::mat4 Translate_m04_00_00;
glm::mat4 Translate_10_00_00;
glm::mat4 Translate_00_05_00;
glm::mat4 Translate_00_04_00;
glm::mat4 Translate_00_10_00;
glm::mat4 Translate_00_00_05;
glm::mat4 Translate_00_00_04;
glm::mat4 Translate_00_00_10;
glm::mat4 Translate_02_00_00;
glm::mat4 Translate_04_00_00;
glm::mat4 Translate_06_00_00;
glm::mat4 Translate_08_00_00;
glm::mat4 Translate_12_00_00;
glm::mat4 Translate_14_00_00;
glm::mat4 Translate_14_04_00;
glm::mat4 Translate_00_04_14;
glm::mat4 Translate_08_00_12;
glm::mat4 Translate_m10_00_10;
glm::mat4 Translate_m10_00_00;
glm::mat4 Translate_m14_00_14;
glm::mat4 Translate_12_00_12;
glm::mat4 Translate_00_00_12;
glm::mat4 Translate_00_00_06;
glm::mat4 Translate_00_00_08;
glm::mat4 Translate_12_06_02;
glm::mat4 Translate_12_02_06;
glm::mat4 Translate_16_m02_00;
glm::mat4 Translate_00_00_m18;
glm::mat4 Translate_20_00_00;
glm::mat4 Translate_24_00_00;
glm::mat4 EmptyTranslateX;

glm::mat4 RotateX_10;
glm::mat4 RotateX_05;
glm::mat4 RotateX_m10;
glm::mat4 RotateX_15;
glm::mat4 RotateX_20;
glm::mat4 RotateX_23;
glm::mat4 RotateX_30;
glm::mat4 RotateX_90;
glm::mat4 RotateX_m90;
glm::mat4 RotateY_30;
glm::mat4 RotateY_90;
glm::mat4 RotateZ_90;
glm::mat4 RotateZ_30;

glm::mat4 Scale_14;

/* Light source position */
float LightPosition1[] = { 0.0, 0.0, 0.0 };
float LightPosition2[] = { 50.0, 50.0, 50.0 };
/* light color */
float LightColor1[] = { 1.0, 1.0, 0.0 };
float LightColor2[] = { 0.1, 0.2, 0.8 };
/* Shading factors */
float ambientFactor = 0.0;
float diffuseFactor = 0.0;
float specularFactor = 0.0;
/* Camera Zoom*/
float cameraZoom = 50.0;
float cameraYPlanet = 0.0;

/* Variables for storing current rotation angle */
float angleX, angleY, angleZ = 0.0f; 
float angle = 0.0f; 
float angle1 = 0.0f; 
float angleX1 = 0.0f; 
float angleY1 = 0.0f; 
float angleY2 = 0.0f; 
float angleY3 = 0.0f; 
float angleY4 = 0.0f; 
float angleY5 = 0.0f; 
float angleY6 = 0.0f; 
float angleY7 = 0.0f; 
float angleYSpace = 0.0f; 

float var = 0.0;

/* Indices to active rotation axes */
enum {Xaxis=0, Yaxis=1, Zaxis=2};
enum {Sun=0, Mercury=1, Venus=2, Earth=3, Moon=4, Mars=5, Jupiter=6, Saturn=7, Uranus=8, Neptune=9, Pluto=10};
int axis = Yaxis;
int obj = Sun;

/* Reference time for animation */
int oldTime = 0;

/* Texture ID */
//GLuint TextureID;
/* Declare as many textures ID you want to use */
/* ... */
GLuint TextureIDsun;
GLuint TextureIDearth;
GLuint TextureIDjupiter;
GLuint TextureIDmars;
GLuint TextureIDmercury;
GLuint TextureIDneptune;
GLuint TextureIDsaturn;
GLuint TextureIDuranus;
GLuint TextureIDvenus;
GLuint TextureIDring;
GLuint TextureIDmoon;
GLuint TextureIDpluto;
GLuint TextureIDspace;

/* Variables for texture handling */
GLuint TextureUniform;
TextureDataPtr Texture;


/*----------------------------------------------------------------*/

/******************************************************************
*
* createQuadMesh
*
* This function creates a simple quad mesh
*
* Input : VBO = pointer to the Vertex buffer object to fill
*         CBO = pointer to the Color buffer object to fill
*         NBO = pointer to the Normal buffer object to fill
*         UVBO = pointer to the UVcoords buffer object to fill
*         IBO = pointer to the Index buffer object to fill
*         rgb = 3D vector containing the color of the object (r=x, g=y, b=z)
*******************************************************************/


void createQuadMesh(GLuint* VBO, GLuint* CBO, GLuint* NBO, GLuint* UVBO, GLuint* IBO, Vector rgb)
{
    GLfloat vertex_buffer_data[] = { /* 8 cube vertices XYZ */
        -1.0, -1.0,  0.0,
         1.0, -1.0,  0.0,
         1.0,  1.0,  0.0,
        -1.0,  1.0,  0.0,
    };
    
    GLfloat color_buffer_data[] = { /* RGB values for 8 vertices */
        rgb.x, rgb.y,  rgb.z,
        rgb.x, rgb.y,  rgb.z,
        rgb.x, rgb.y,  rgb.z,
        rgb.x, rgb.y,  rgb.z,
    };
    
    GLfloat normal_buffer_data[] = { /* Normal coords for 8 vertices */
        0.0, 0.0,  -1.0,
        0.0, 0.0,  -1.0,
        0.0, 0.0,  -1.0,
        0.0, 0.0,  -1.0,
    };
    
    GLfloat uv_buffer_data[] = { /* UV coords for 8 vertices */
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
    };

    GLushort index_buffer_data[] = { /* Indices of 2 triangles */
        0, 1, 2,
        2, 3, 0,
    };

    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, *VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data, GL_STATIC_DRAW);
    
    glGenBuffers(1, CBO);
    glBindBuffer(GL_ARRAY_BUFFER, *CBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(color_buffer_data), color_buffer_data, GL_STATIC_DRAW);
    
    glGenBuffers(1, NBO);
    glBindBuffer(GL_ARRAY_BUFFER, *NBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normal_buffer_data), normal_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, UVBO);
    glBindBuffer(GL_ARRAY_BUFFER, *UVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uv_buffer_data), uv_buffer_data, GL_STATIC_DRAW);
    
    glGenBuffers(1, IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index_buffer_data), index_buffer_data, GL_STATIC_DRAW);
}


/******************************************************************
*
* readMeshFile
*
* This function read the content of an OBJ file and then fill the
* buffer objects with the data
*
* Input : filename = name of file.obj
*         scale = scale factor applied to the vertices
*         VBO = pointer to the Vertex buffer object to fill
*         CBO = pointer to the Color buffer object to fill
*         NBO = pointer to the Normal buffer object to fill
*         UVBO = pointer to the UVcoords buffer object to fill
*         IBO = pointer to the Index buffer object to fill
*         rgb = 3D vector containing the color of the object (r=x, g=y, b=z)
*******************************************************************/

void readMeshFile(char* filename, float scale, GLuint* VBO, GLuint* CBO, GLuint* NBO, GLuint* UVBO, GLuint* IBO, Vector rgb)
{
    int i, j;
    
    /* Structure for loading of OBJ data */
    obj_scene_data data;

    /* Load first OBJ model */
    int success = parse_obj_scene(&data, filename);

    if(!success)
        printf("Could not load file. Exiting.\n");

    /*  Copy mesh data from structs into appropriate arrays */
    int indx = data.face_count;

    GLushort* index_buffer_data = (GLushort*) calloc (indx*3, sizeof(GLushort));
    GLfloat* vertex_buffer_data = (GLfloat*) calloc (indx*9, sizeof(GLfloat));
    GLfloat* color_buffer_data = (GLfloat*) calloc (indx*9, sizeof(GLfloat));
    GLfloat* normal_buffer_data = (GLfloat*) calloc (indx*9, sizeof(GLfloat));
    GLfloat* uv_buffer_data = (GLfloat*) calloc (indx*6, sizeof(GLfloat));

    /* for each triangle... */
    for(i=0; i<indx; i++)
    {
        int offset3D = i*9;
        int offset2D = i*6;

        /* fill VBO for this triangle (x,y,z coords for 3 vertices = 9 values) */
        for(j=0; j<3; j++)
        {
            /* Index of current vertex */
            int idVert = (GLushort)(*data.face_list[i]).vertex_index[j];      
            vertex_buffer_data[offset3D + j*3 ] = (GLfloat)(*data.vertex_list[idVert]).e[0]*scale;
            vertex_buffer_data[offset3D + j*3 + 1] = (GLfloat)(*data.vertex_list[idVert]).e[1]*scale;
            vertex_buffer_data[offset3D + j*3 + 2] = (GLfloat)(*data.vertex_list[idVert]).e[2]*scale;
        }

        /* fill Normal buffer for this triangle */
        if( (*data.face_list[i]).normal_index[0] != -1 )
        {
            for(j=0; j<3; j++)
            {
                int idNorm = (GLushort)(*data.face_list[i]).normal_index[j]; 
                normal_buffer_data[offset3D + j*3 ] = (GLfloat)(*data.vertex_normal_list[idNorm]).e[0];
                normal_buffer_data[offset3D + j*3 + 1] = (GLfloat)(*data.vertex_normal_list[idNorm]).e[1];
                normal_buffer_data[offset3D + j*3 + 2] = (GLfloat)(*data.vertex_normal_list[idNorm]).e[2];
            }
        }
        else
        {
            for(j=0; j<3; j++)
            {
                normal_buffer_data[offset3D + j*3 ] = vertex_buffer_data[offset3D + j*3 ];
                normal_buffer_data[offset3D + j*3 + 1] = vertex_buffer_data[offset3D + j*3 + 1];
                normal_buffer_data[offset3D + j*3 + 2] = vertex_buffer_data[offset3D + j*3 + 2];
            }
        }

        /* fill UV buffer for this triangle */
        if( (*data.face_list[i]).texture_index[0] != -1 )
        {
            for(j=0; j<3; j++)
            {
                int idUV = (GLushort)(*data.face_list[i]).texture_index[j];
                uv_buffer_data[offset2D + j*2 ] = (GLfloat)(*data.vertex_texture_list[idUV]).e[0];
                uv_buffer_data[offset2D + j*2 + 1] = (GLfloat)(*data.vertex_texture_list[idUV]).e[1];
            }
        }

        /* fill Color buffer for this triangle */
        for(j=0; j<3; j++)
        {
            color_buffer_data[offset3D + j*3 ] = (GLfloat)(rgb.x);
            color_buffer_data[offset3D + j*3 + 1] = (GLfloat)(rgb.y);
            color_buffer_data[offset3D + j*3 + 2] = (GLfloat)(rgb.z);
        }

        /* Fill indices buffer for this triangles (3 indices) */
        index_buffer_data[i*3] = i*3;      
        index_buffer_data[i*3+1] = i*3+1;
        index_buffer_data[i*3+2] = i*3+2;
    }

    /* Create buffer objects and load data into buffers*/
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, *VBO);
    glBufferData(GL_ARRAY_BUFFER, data.face_count*9*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, CBO);
    glBindBuffer(GL_ARRAY_BUFFER, *CBO);
    glBufferData(GL_ARRAY_BUFFER, data.face_count*9*sizeof(GLfloat), color_buffer_data, GL_STATIC_DRAW);
    
    glGenBuffers(1, NBO);
    glBindBuffer(GL_ARRAY_BUFFER, *NBO);
    glBufferData(GL_ARRAY_BUFFER, data.face_count*9*sizeof(GLfloat), normal_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, UVBO);
    glBindBuffer(GL_ARRAY_BUFFER, *UVBO);
    glBufferData(GL_ARRAY_BUFFER, data.face_count*6*sizeof(GLfloat), uv_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.face_count*3*sizeof(GLushort), index_buffer_data, GL_STATIC_DRAW);
}

/******************************************************************
*
* Display
*
* This function is called when the content of the window needs to be
* drawn/redrawn. It has been specified through 'glutDisplayFunc()';
* Enable vertex attributes, create binding between C program and 
* attribute name in shader, provide data for uniform variables
*
*******************************************************************/

void Display()
{
    int i;

    /* Clear window; color specified in 'Initialize()' */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* For each object in the scene */
    for(i = 0; i < nbObjects; i++)
    {

        /* Activate first (and only) texture unit */
        glActiveTexture(GL_TEXTURE0);

        /* Bind current texture  */
        //done inside if clauses below
        //glBindTexture(GL_TEXTURE_2D, TextureIDsun);

        /* Get texture uniform handle from fragment shader */
        TextureUniform  = glGetUniformLocation(ShaderProgram, "myTextureSampler");
        /* Set location of uniform sampler variable */
        glUniform1i(TextureUniform, 0);

        /* Bind buffer with vertex data of currently active object */
        glEnableVertexAttribArray(vPosition);
        glBindBuffer(GL_ARRAY_BUFFER, VBOs[i]);
        glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

        /* Bind color buffer */
        glEnableVertexAttribArray(vColor);
        glBindBuffer(GL_ARRAY_BUFFER, CBOs[i]);
        glVertexAttribPointer(vColor, 3, GL_FLOAT,GL_FALSE, 0, 0);

        /* Bind normal buffer */
        glEnableVertexAttribArray(vNormal);
        glBindBuffer(GL_ARRAY_BUFFER, NBOs[i]);
        glVertexAttribPointer(vNormal, 3, GL_FLOAT,GL_FALSE, 0, 0);   

        /* Bind uv buffer */
        glEnableVertexAttribArray(vUV);
        glBindBuffer(GL_ARRAY_BUFFER, UVBOs[i]);
        glVertexAttribPointer(vUV, 2, GL_FLOAT,GL_FALSE, 0, 0);

        /* Bind index buffer */
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBOs[i]);

        GLint size;
        glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);

        /* Associate program with uniform shader matrices */
        GLint projectionUniform = glGetUniformLocation(ShaderProgram, "ProjectionMatrix");
        if (projectionUniform == -1)
        {
            fprintf(stderr, "Could not bind uniform ProjectionMatrix\n");
            exit(-1);
        }
        glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, &ProjectionMatrix[0][0]);

        GLint ViewUniform = glGetUniformLocation(ShaderProgram, "ViewMatrix");
        if (ViewUniform == -1)
        {
            fprintf(stderr, "Could not bind uniform ViewMatrix\n");
            exit(-1);
        }
        glUniformMatrix4fv(ViewUniform, 1, GL_FALSE, &ViewMatrix[0][0]);

        GLint RotationUniform = glGetUniformLocation(ShaderProgram, "ModelMatrix");
        if (RotationUniform == -1)
        {
            fprintf(stderr, "Could not bind uniform ModelMatrix\n");
            exit(-1);
        }
        glUniformMatrix4fv(RotationUniform, 1, GL_FALSE, &ModelMatrix[i][0][0]);

        GLint LightPos1Uniform = glGetUniformLocation(ShaderProgram, "LightPosition1");
        glUniform3f(LightPos1Uniform, LightPosition1[0], LightPosition1[1], LightPosition1[2]);

        GLint LightCol1Uniform = glGetUniformLocation(ShaderProgram, "LightColor1");
        glUniform3f(LightCol1Uniform, LightColor1[0], LightColor1[1], LightColor1[2]);

        //4 lines of code for second light source
        GLint LightPos2Uniform = glGetUniformLocation(ShaderProgram, "LightPosition2");
        glUniform3f(LightPos2Uniform, LightPosition2[0], LightPosition2[1], LightPosition2[2]);

        GLint LightCol2Uniform = glGetUniformLocation(ShaderProgram, "LightColor2");
        glUniform3f(LightCol2Uniform, LightColor2[0], LightColor2[1], LightColor2[2]);

        GLint AmbientFactorUniform = glGetUniformLocation(ShaderProgram, "AmbientFactor");
        glUniform1f(AmbientFactorUniform, ambientFactor );

        GLint DiffuseFactorUniform = glGetUniformLocation(ShaderProgram, "DiffuseFactor");
        glUniform1f(DiffuseFactorUniform, diffuseFactor);

        GLint SpecularFactorUniform = glGetUniformLocation(ShaderProgram, "SpecularFactor");
        glUniform1f(SpecularFactorUniform, specularFactor);

        /* Uniform integer used to enable/disable texture mapping */
        GLint UseTexUniform = glGetUniformLocation(ShaderProgram, "UseTexture");
        if(i == 0)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDsun);
            /* Disable texture mapping for first object (sphere) */
            glUniform1i(UseTexUniform, 1);
            /* Use Wireframe rendering */
            //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }
        if(i == 1)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDearth);
            /* Enable texture mapping for object */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 2)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmoon);
            /* Enable texture mapping for object */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 3)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmercury);
            /* Enable texture mapping for object */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 4)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDvenus);
            /* Enable texture mapping for object */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 5)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmars);
            /* Enable texture mapping for other  (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 6)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDjupiter);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 7)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDsaturn);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 9)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDuranus);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 10)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDneptune);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 11)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDpluto);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 12)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmoon);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 13)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmoon);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 14)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmoon);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 15)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmoon);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 16)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmoon);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 17)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmoon);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 8)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDmoon);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 18)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDspace);
            /* Enable texture mapping for other object (quad) */
            glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if(i == 19)
        {
            glBindTexture(GL_TEXTURE_2D, TextureIDring);
            /* Enable texture mapping for other object (quad) */
            //glUniform1i(UseTexUniform, 1);
            /* Use filled polygons rendering */
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            //To enable alpha blending, you must add these lines before calling glDrawElements():
            glEnable (GL_BLEND);
            glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            /* Issue draw command, using indexed triangle list */
            glDrawElements(GL_TRIANGLES, size/sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
            //After glDrawElements(), deactivate alpha blending with:
            glDisable (GL_BLEND);
        }
        /* Issue draw command, using indexed triangle list */
        glDrawElements(GL_TRIANGLES, size/sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
        //After glDrawElements(), deactivate alpha blending with:

        /* Disable attributes */
        glDisableVertexAttribArray(vPosition);
        glDisableVertexAttribArray(vColor);
        glDisableVertexAttribArray(vNormal);
        glDisableVertexAttribArray(vUV);
    }
    /* Swap between front and back buffer */
    glutSwapBuffers();
}

/******************************************************************
*
* Mouse
*
* Function is called on mouse button press; has been seta
* with glutMouseFunc(), x and y specify mouse coordinates,
* but are not used here.
*
*******************************************************************/

void Mouse(int button, int state, int x, int y) 
{
    if(state == GLUT_DOWN)
    {
        /* ... */
    }
    glutPostRedisplay();
}

/******************************************************************
*
* Keyboard
*
* Function to be called on key press in window; set by
* glutKeyboardFunc(); x and y specify mouse position on keypress;
* not used in this example 
*
*******************************************************************/

void Keyboard(unsigned char key, int x, int y)
{
    /* Here we change the rotation axis when a key is pressed */
    switch( key )
    {
        case 'a':
            ambientFactor = fmod((ambientFactor + 0.01), 1);
            break;

        case 'd':
            diffuseFactor = fmod((diffuseFactor + 0.01), 1);
            break;

        case 's':
            specularFactor = fmod((specularFactor + 0.01), 1);
            break;

        case '-':
            cameraZoom = fmod((cameraZoom + 0.2), 50);
            break;

        case '+':
            cameraZoom = fmod((cameraZoom - 0.2), 50);
            break;

        case 'C':
            cameraYPlanet = fmod((cameraYPlanet - 0.01), 3);
            break;

        case 'c':
            cameraYPlanet = fmod((cameraYPlanet + 0.01), 3);
            break;

        case '1':
            obj = Sun;
            printf("Sun selected \n");
            break;

        case '2':
            obj = Mercury;
            printf("Mercury selected \n");
            break;
            
        case '3':
            obj = Venus;
            printf("Venus selected \n");
            break;
            
        case '4':
            obj = Earth;
            printf("Earth selected \n");
            break;
            
        case '5':
            obj = Mars;
            printf("Mars selected \n");
            break;
            
        case '6':
            obj = Jupiter;
            printf("Jupiter selected \n");
            break;
            
        case '7':
            obj = Saturn;
            printf("Saturn selected \n");
            break;
            
        case '8':
            obj = Uranus;
            printf("Uranus selected \n");
            break;
            
        case '9':
            obj = Neptune;
            printf("Neptune selected \n");
            break;

        case '0':
            obj = Pluto;
            printf("Pluto selected \n");
            break;

        case 'q': case 'Q':
            exit(0);
                break;
    }

    glutPostRedisplay();
}


/******************************************************************
*
* OnIdle
*
* Function executed when no other events are processed; set by
* call to glutIdleFunc(); holds code for animation  
*
*******************************************************************/

void OnIdle()
{
    /* Determine delta time between two frames to ensure constant animation */
    int newTime = glutGet(GLUT_ELAPSED_TIME);
    int delta = newTime - oldTime;
    oldTime = newTime;

    /* Initialize a rotation matrix (identity)*/
    glm::mat4 RotationMatrixAnim = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnim1 = glm::mat4(1.0f);
    glm::mat4 CameraMatrixAnim1 = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimY = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimY1 = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimY2 = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimY3 = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimY4 = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimY5 = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimY6 = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimY7 = glm::mat4(1.0f);
    glm::mat4 RotationMatrixAnimYSpace = glm::mat4(1.0f);
    
    if(anim)
    {
        /* Increment rotation angle*/
        angle = fmod(angle + delta/30000.0, 360.0);
        angle1 = fmod(angle1 + delta/11000.0, 360.0);
        angleY = fmod(angleY + delta/18000.0, 360.0);
        angleY4 = fmod(angleY4 + delta/9000.0, 360.0);
        angleY1 = fmod(angleY1 + delta/7000.0, 360.0);
        angleY3 = fmod(angleY3 + delta/12000.0, 360.0);
        angleY2 = fmod(angleY2 + delta/20000.0, 360.0);
        angleY5 = fmod(angleY5 + delta/6000.0, 360.0);
        angleY6 = fmod(angleY6 + delta/8000.0, 360.0);
        angleY7 = fmod(angleY7 + delta/25000.0, 360.0);
        angleYSpace = fmod(angleY7 + delta/40000.0, 360.0);

        /*update rotation matrix */
        RotationMatrixAnim = glm::rotate(glm::mat4(1.0f), angle, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnim1 = glm::rotate(glm::mat4(1.0f), angle1, glm::vec3(0.0f, 1.0f, 0.0f) );
        CameraMatrixAnim1 = glm::rotate(glm::mat4(1.0f), angle1, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimY = glm::rotate(glm::mat4(1.0f), angleY, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimY4 = glm::rotate(glm::mat4(1.0f), angleY4, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimY1 = glm::rotate(glm::mat4(1.0f), angleY1, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimY2 = glm::rotate(glm::mat4(1.0f), angleY2, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimY3 = glm::rotate(glm::mat4(1.0f), angleY3, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimY5 = glm::rotate(glm::mat4(1.0f), angleY5, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimY6 = glm::rotate(glm::mat4(1.0f), angleY6, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimY7 = glm::rotate(glm::mat4(1.0f), angleY7, glm::vec3(0.0f, 1.0f, 0.0f) );
        RotationMatrixAnimYSpace = glm::rotate(glm::mat4(1.0f), angleY7, glm::vec3(0.0f, 1.0f, 0.0f) );
    }

    /* Model matrix of the first object: Sun */
    ModelMatrix[0] = RotationMatrixAnim;
    /* Earth */
    ModelMatrix[1] = RotationMatrixAnim1 * Translate_08_00_00 * RotateX_23 * RotationMatrixAnimY4;
    /* Moon */
    ModelMatrix[2] = RotationMatrixAnim1 * Translate_08_00_00 * RotateX_20 * RotationMatrixAnim1 * RotateX_20 * Translate_01_00_00;
    /* Mercury */
    ModelMatrix[3] = RotationMatrixAnim * Translate_02_00_00 * RotationMatrixAnimY4;
    /* Venus */
    ModelMatrix[4] = RotateX_10 * RotationMatrixAnimY1 * Translate_04_00_00 * RotationMatrixAnimY4;
    /* Mars */
    ModelMatrix[5] = RotationMatrixAnimY2 * Translate_m10_00_00 * RotationMatrixAnimY4;
    /* Jupiter*/
    ModelMatrix[6] = RotateX_05 * RotationMatrixAnimY3 * Translate_00_04_14 * RotationMatrixAnimY3;
    /* Ganymede */
    ModelMatrix[12] = RotateX_05 * RotationMatrixAnimY3 * Translate_00_04_14 * RotationMatrixAnimY5 * RotateX_20 * Translate_03_00_00;
    /* Callisto */
    ModelMatrix[14] = RotateX_05 * RotationMatrixAnimY3 * Translate_00_04_14 * RotationMatrixAnimY4 * RotateX_30 * Translate_00_00_04;
    /* Io */
    ModelMatrix[15] = RotateX_05 * RotationMatrixAnimY3 * Translate_00_04_14 * RotateX_30 * RotationMatrixAnimY6 * Translate_04_00_00;
    /* Europa */
    ModelMatrix[16] = RotateX_05 * RotationMatrixAnimY3 * Translate_00_04_14 * RotateX_05 * RotationMatrixAnimY * Translate_00_00_05;
    /* Saturn*/
    ModelMatrix[7] = RotationMatrixAnimY2 * Translate_16_m02_00 * RotateX_10 * RotationMatrixAnimY3;
    /* Titan */
    ModelMatrix[13] = RotationMatrixAnimY2 * Translate_16_m02_00 * RotateX_10 * RotationMatrixAnimY3 * Translate_03_00_00;
    /* Saturn ring */
    ModelMatrix[19] =  RotationMatrixAnimY2 * Translate_16_m02_00 * Scale_14 * RotateX_10 * RotationMatrixAnimY3 * RotateX_90;
    /* Uranus */
    ModelMatrix[9] = RotationMatrixAnimY * Translate_00_00_m18 * RotationMatrixAnimY3;
    /* Titania */
    ModelMatrix[8] = RotationMatrixAnimY * Translate_00_00_m18 * RotationMatrixAnimY3 * Translate_02_00_00;
    /* Neptune */
    ModelMatrix[10] = RotateX_10 * RotationMatrixAnimY7 * Translate_20_00_00 * RotationMatrixAnimY1;
    /* Triton */
    ModelMatrix[17] = RotateX_10 * RotationMatrixAnimY7 * Translate_20_00_00 * RotationMatrixAnimY3 * Translate_02_00_00;
    /* Pluto */
    ModelMatrix[11] = RotateX_05 * RotationMatrixAnim * Translate_24_00_00 * RotationMatrixAnimY3;
    /* skydome */
    ModelMatrix[18] = RotationMatrixAnimYSpace;

    /* Set all different camera views */
    if(obj == Earth){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angle1)*12, cameraYPlanet, sin(-angle1)*12+cameraZoom), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }
    else if(obj == Sun){
        ViewMatrix = glm::lookAt(glm::vec3(0, -cameraYPlanet*15, cameraZoom), glm::vec3(0, 0, 0), glm::vec3(0, 1, -1) );
    }
    else if(obj == Mercury){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angle)*4, cameraYPlanet, sin(-angle)*4+cameraZoom/10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }
    else if(obj == Venus){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angleY1)*7, sin(-angleY1)*8*sin(-0.174)+cameraYPlanet, sin(-angleY1)*7+cameraZoom), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }
    else if(obj == Mars){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angleY2-3.14)*13, cameraYPlanet*0.7, sin(-angleY2-3.14)*13+cameraZoom/10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }
    else if(obj == Jupiter){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angleY3)*22, -1.5+cameraYPlanet, sin(-angleY3)*22+cameraZoom), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }
    else if(obj == Saturn){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angleY2)*21, cameraYPlanet-2, sin(-angleY2)*21+cameraZoom), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }
    else if(obj == Uranus){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angleY-1.57)*23, -1+cameraYPlanet, sin(-angleY-1.57)*23+cameraZoom), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }
    else if(obj == Neptune){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angleY7)*24, sin(-angleY7)*24*sin(-0.174)+cameraYPlanet-1.5, sin(-angleY7)*24+cameraZoom), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }
    else if(obj == Pluto){
        ViewMatrix = glm::lookAt(glm::vec3(cos(-angle)*26, sin(-angle)*26*sin(-0.087)+cameraYPlanet/10, sin(-angle)*26+cameraZoom/10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0) );
    }

    /* Issue display refresh */
    glutPostRedisplay();
}



/******************************************************************
*
* AddShader
*
* This function creates and adds individual shaders
*
*******************************************************************/

void AddShader(GLuint ShaderProgram, const char* ShaderCode, GLenum ShaderType)
{
    /* Create shader object */
    GLuint ShaderObj = glCreateShader(ShaderType);

    if (ShaderObj == 0) 
    {
        fprintf(stderr, "Error creating shader type %d\n", ShaderType);
        exit(0);
    }

    /* Associate shader source code string with shader object */
    glShaderSource(ShaderObj, 1, &ShaderCode, NULL);

    GLint success = 0;
    GLchar InfoLog[1024];

    /* Compile shader source code */
    glCompileShader(ShaderObj);
    glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);

    if (!success) 
    {
        glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
        fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
        exit(1);
    }

    /* Associate shader with shader program */
    glAttachShader(ShaderProgram, ShaderObj);
}


/******************************************************************
*
* CreateShaderProgram
*
* This function creates the shader program; vertex and fragment
* shaders are loaded and linked into program; final shader program
* is put into the rendering pipeline 
*
*******************************************************************/

void CreateShaderProgram()
{
    /* Allocate shader object */
    ShaderProgram = glCreateProgram();

    if (ShaderProgram == 0) 
    {
        fprintf(stderr, "Error creating shader program\n");
        exit(1);
    }

    /* Load shader code from file */
    VertexShaderString = LoadShader("shaders/vertexshader.vs");
    FragmentShaderString = LoadShader("shaders/fragmentshader.fs");

    /* Separately add vertex and fragment shader to program */
    AddShader(ShaderProgram, VertexShaderString, GL_VERTEX_SHADER);
    AddShader(ShaderProgram, FragmentShaderString, GL_FRAGMENT_SHADER);

    GLint Success = 0;
    GLchar ErrorLog[1024];

    /* Link shader code into executable shader program */
    glLinkProgram(ShaderProgram);

    /* Check results of linking step */
    glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

    if (Success == 0) 
    {
        glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    /* Check if shader program can be executed */ 
    glValidateProgram(ShaderProgram);
    glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);

    if (!Success) 
    {
        glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    /* Put linked shader program into drawing pipeline */
    glUseProgram(ShaderProgram);
}


/******************************************************************
*
* SetupTexture
*
* This function is called to load the texture and initialize
* texturing parameters
*
* Input: TextureID = id of the texture to setup
*        filename = path to bitmap file to read
*******************************************************************/

void SetupTexture(GLuint *TextureID, char* filename)
{
    /* Allocate texture container */
    Texture = (TextureDataPtr)malloc(sizeof(TextureDataPtr));

    int success = LoadTexture(filename, Texture);
    if (!success)
    {
        printf("Error loading texture. Exiting.\n");
        exit(-1);
    }

    /* Create texture name and store in handle */
    glGenTextures(1, TextureID);

    /* Bind texture */
    glBindTexture(GL_TEXTURE_2D, *TextureID);

    /* Load texture image into memory */
    glTexImage2D(GL_TEXTURE_2D,     /* Target texture */
                 0,                 /* Base level */
                 GL_RGB,            /* Each element is RGB triple */
                 Texture->width,    /* Texture dimensions */
                 Texture->height,
                 0,                 /* Border should be zero */
                 GL_BGR,            /* Data storage format for BMP file */
                 GL_UNSIGNED_BYTE,  /* Type of pixel data, one byte per channel */
                 Texture->data);    /* Pointer to image data  */

    /* Next set up texturing parameters */

    /* Repeat texture on edges when tiling */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    /* Linear interpolation for magnification */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    /* Trilinear MIP mapping for minification */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

    /* Note: MIP mapping not visible due to fixed, i.e. static camera */
}

/******************************************************************
*
* Initialize
*
* This function is called to initialize rendering elements, setup
* vertex buffer objects, and to setup the vertex and fragment shader;
* meshes are loaded from files in OBJ format; data is copied from
* structures into vertex and index arrays
*
*******************************************************************/

void Initialize()
{   
    /* Create 2 cube meshes */
    //createCubeMesh(&VBOs[0], &IBOs[0], &CBOs[0]);
    //createCubeMesh(&VBOs[1], &IBOs[1], &CBOs[1]);
    //createCubeMesh(&VBOs[2], &IBOs[2], &CBOs[2]);

    /* create n meshes*/
    Vector color;
    color.x = 1.0; color.y = 1.0; color.z = 1.0;
    char* filename = "models/uvsphere.obj";
    //Sun
    readMeshFile(filename, 1.0f, &VBOs[0], &CBOs[0], &NBOs[0], &UVBOs[0], &IBOs[0], color);
    //Earth
    readMeshFile(filename, 0.25f, &VBOs[1], &CBOs[1], &NBOs[1], &UVBOs[1], &IBOs[1], color);
    //Moon
    readMeshFile(filename, 0.05f, &VBOs[2], &CBOs[2], &NBOs[2], &UVBOs[2], &IBOs[2], color);
    //Mercury
    readMeshFile(filename, 0.08f, &VBOs[3], &CBOs[3], &NBOs[3], &UVBOs[3], &IBOs[3], color);
    //Venus
    readMeshFile(filename, 0.24f, &VBOs[4], &CBOs[4], &NBOs[4], &UVBOs[4], &IBOs[4], color);
    //Mars
    readMeshFile(filename, 0.11f, &VBOs[5], &CBOs[5], &NBOs[5], &UVBOs[5], &IBOs[5], color);
    //Jupiter
    readMeshFile(filename, 0.7f, &VBOs[6], &CBOs[6], &NBOs[6], &UVBOs[6], &IBOs[6], color);
    //Saturn
    readMeshFile(filename, 0.5f, &VBOs[7], &CBOs[7], &NBOs[7], &UVBOs[7], &IBOs[7], color);
    //Uranus
    readMeshFile(filename, 0.4f, &VBOs[9], &CBOs[9], &NBOs[9], &UVBOs[9], &IBOs[9], color);
    //Neptune
    readMeshFile(filename, 0.4f, &VBOs[10], &CBOs[10], &NBOs[10], &UVBOs[10], &IBOs[10], color);
    //Pluto
    readMeshFile(filename, 0.02f, &VBOs[11], &CBOs[11], &NBOs[11], &UVBOs[11], &IBOs[11], color);
    //Ganymede
    readMeshFile(filename, 0.1f, &VBOs[12], &CBOs[12], &NBOs[12], &UVBOs[12], &IBOs[12], color);
    //Titan
    readMeshFile(filename, 0.09f, &VBOs[13], &CBOs[13], &NBOs[13], &UVBOs[13], &IBOs[13], color);
    //Callisto
    readMeshFile(filename, 0.07f, &VBOs[14], &CBOs[14], &NBOs[14], &UVBOs[14], &IBOs[14], color);
    //Io
    readMeshFile(filename, 0.06f, &VBOs[15], &CBOs[15], &NBOs[15], &UVBOs[15], &IBOs[15], color);
    //Europa
    readMeshFile(filename, 0.04f, &VBOs[16], &CBOs[16], &NBOs[16], &UVBOs[16], &IBOs[16], color);
    //Triton
    readMeshFile(filename, 0.03f, &VBOs[17], &CBOs[17], &NBOs[17], &UVBOs[17], &IBOs[17], color);
    //Titania
    readMeshFile(filename, 0.01f, &VBOs[8], &CBOs[8], &NBOs[8], &UVBOs[8], &IBOs[8], color);
    //Skydome
    readMeshFile(filename, 55.0f, &VBOs[18], &CBOs[18], &NBOs[18], &UVBOs[18], &IBOs[18], color);

    //Saturn ring insert quad at the end otherwise transparency doesn't work
    createQuadMesh(&VBOs[19], &CBOs[19], &NBOs[19], &UVBOs[19], &IBOs[19], color);

    /* Set background (clear) color to blue */ 
    glClearColor(0.0, 0.0, 0.03, 0.0);

    /* Enable depth testing */
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);    

    /* Setup shaders and shader program */
    CreateShaderProgram();  

    /* Setup texture */
    SetupTexture(&TextureIDsun, "data/sun_tex.bmp");
    /* Setup as many textures you want to use */
    /* ... */
    SetupTexture(&TextureIDearth, "data/earth_tex.bmp");
    SetupTexture(&TextureIDjupiter, "data/jupiter_tex.bmp");
    SetupTexture(&TextureIDring, "data/ring_tex.bmp");
    SetupTexture(&TextureIDmars, "data/mars_tex.bmp");
    SetupTexture(&TextureIDmercury, "data/mercury_tex.bmp");
    SetupTexture(&TextureIDneptune, "data/neptune_tex.bmp");
    SetupTexture(&TextureIDsaturn, "data/saturn_tex.bmp");
    SetupTexture(&TextureIDuranus, "data/uranus_tex.bmp");
    SetupTexture(&TextureIDvenus, "data/venus_tex.bmp");
    SetupTexture(&TextureIDmoon, "data/moon_tex.bmp");
    SetupTexture(&TextureIDuranus, "data/uranus_tex.bmp");
    SetupTexture(&TextureIDneptune, "data/neptune_tex.bmp");
    SetupTexture(&TextureIDpluto, "data/pluto_tex.bmp");
    SetupTexture(&TextureIDspace, "data/space_tex.bmp");

    /* Initialize matrices for translating */
    Translate_01_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 0.0f, 0.0f) );
    Translate_03_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(3.0f, 0.0f, 0.0f) );
    Translate_05_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, 0.0f, 0.0f) );
    Translate_08_02_00 = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, 2.0f, 0.0f) );
    Translate_00_05_40 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.5f, 40.0f) );
    Translate_07_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, 0.0f, 0.0f) );
    Translate_m04_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(-4.0f, 0.0f, 0.0f) );
    Translate_10_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(10.0f, 0.0f, 0.0f) );
    Translate_00_05_00 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 5.0f, 0.0f) );
    Translate_00_04_00 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 4.0f, 0.0f) );
    Translate_00_10_00 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 10.0f, 0.0f) );
    Translate_00_00_05 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 5.0f) );
    Translate_00_00_04 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 4.0f) );
    Translate_00_00_10 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 10.0f) );
    Translate_02_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(2.0f, 0.0f, 0.0f) );
    Translate_04_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(4.0f, 0.0f, 0.0f) );
    Translate_06_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(6.0f, 0.0f, 0.0f) );
    Translate_08_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, 0.0f, 0.0f) );
    Translate_12_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(12.0f, 0.0f, 0.0f) );
    Translate_14_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(14.0f, 0.0f, 0.0f) );
    Translate_14_04_00 = glm::translate(glm::mat4(1.0f), glm::vec3(14.0f, 0.4f, 0.0f) );
    Translate_00_04_14 = glm::translate(glm::mat4(1.0f), glm::vec3(14.0f, 0.4f, 0.0f) );
    Translate_08_00_12 = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, 0.0f, 12.0f) );
    Translate_m10_00_10 = glm::translate(glm::mat4(1.0f), glm::vec3(-10.0f, 0.0f, 10.0f) );
    Translate_m10_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(-10.0f, 0.0f, 0.0f) );
    Translate_m14_00_14 = glm::translate(glm::mat4(1.0f), glm::vec3(-14.0f, 0.0f, 14.0f) );
    Translate_12_00_12 = glm::translate(glm::mat4(1.0f), glm::vec3(12.0f, 0.0f, 12.0f) );
    Translate_00_00_04 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 4.0f) );
    Translate_00_00_06 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 6.0f) );
    Translate_00_00_08 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 8.0f) );
    Translate_00_00_10 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 10.0f) );
    Translate_00_00_12 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 12.0f) );
    Translate_12_06_02 = glm::translate(glm::mat4(1.0f), glm::vec3(12.0f, 6.0f, 2.0f) );
    Translate_12_02_06 = glm::translate(glm::mat4(1.0f), glm::vec3(12.0f, 2.0f, 6.0f) );
    Translate_16_m02_00 = glm::translate(glm::mat4(1.0f), glm::vec3(16.0f, -2.0f, 0.0f) );
    Translate_00_00_m18 = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -18.0f) );
    Translate_20_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(20.0f, 0.0f, 0.0f) );
    Translate_24_00_00 = glm::translate(glm::mat4(1.0f), glm::vec3(24.0f, 0.0f, 0.0f) );

    /* Initialize matrices for scaling */

    /* initialize matrices for rotating angle in radians*/
    RotateX_90 = glm::rotate(glm::mat4(1.0f), 1.57f, glm::vec3(1.0f, 0.0f, 0.0f) );
    RotateX_05 = glm::rotate(glm::mat4(1.0f), 0.087f, glm::vec3(1.0f, 0.0f, 0.0f) );
    RotateX_m90 = glm::rotate(glm::mat4(1.0f), -1.57f, glm::vec3(1.0f, 0.0f, 0.0f) );
    RotateY_90 = glm::rotate(glm::mat4(1.0f), 1.57f, glm::vec3(0.0f, 1.0f, 0.0f) );
    RotateZ_90 = glm::rotate(glm::mat4(1.0f), 1.57f, glm::vec3(0.0f, 0.0f, 1.0f) );
    RotateZ_30 = glm::rotate(glm::mat4(1.0f), 0.523f, glm::vec3(0.0f, 0.0f, 1.0f) );
    RotateY_30 = glm::rotate(glm::mat4(1.0f), 0.523f, glm::vec3(0.0f, 1.0f, 0.0f) );
    RotateX_30 = glm::rotate(glm::mat4(1.0f), 0.523f, glm::vec3(1.0f, 0.0f, 0.0f) );
    RotateX_10 = glm::rotate(glm::mat4(1.0f), 0.174f, glm::vec3(1.0f, 0.0f, 0.0f) );
    RotateX_m10 = glm::rotate(glm::mat4(1.0f), -0.174f, glm::vec3(1.0f, 0.0f, 0.0f) );
    RotateX_15 = glm::rotate(glm::mat4(1.0f), 0.261f, glm::vec3(1.0f, 0.0f, 0.0f) );
    RotateX_20 = glm::rotate(glm::mat4(1.0f), 0.349f, glm::vec3(1.0f, 0.0f, 0.0f) );
    RotateX_23 = glm::rotate(glm::mat4(1.0f), 0.401f, glm::vec3(1.0f, 0.0f, 0.0f) );

    Scale_14 = glm::scale(glm::mat4(1.0f), glm::vec3(1.4f, 1.4f, 1.4f) );

    /* Set projection transform */
    winWidth = 1400.0f;
    winHeight = 800.0f;
    float aspect = winWidth / winHeight;
    float nearPlane = 1.0; 
    float farPlane = 120.0;
    float fovy = 45.0f;

    /* Set viewing transform */
    ProjectionMatrix = glm::perspective(fovy, aspect, nearPlane, farPlane);
    ViewMatrix = glm::lookAt(glm::vec3(0, 5, 20), glm::vec3(0, 0, 0), glm::vec3(0, 1, -1) );
}


/******************************************************************
*
* main
*
* Main function to setup GLUT, GLEW, and enter rendering loop
*
*******************************************************************/

int main(int argc, char** argv)
{
    /* Initialize GLUT; set double buffered window and RGBA color model */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(winWidth, winHeight);
    glutInitWindowPosition(200, 100);
    glutCreateWindow("CG Proseminar - Transformations");

    /* Initialize GL extension wrangler */
    GLenum res = glewInit();
    if (res != GLEW_OK) 
    {
        fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
        return 1;
    }

    /* Setup scene and rendering parameters */
    Initialize();

    /* Specify callback functions;enter GLUT event processing loop, 
     * handing control over to GLUT */
    glutIdleFunc(OnIdle);
    glutDisplayFunc(Display);
    glutKeyboardFunc(Keyboard); 
    glutMouseFunc(Mouse);

    glutMainLoop();

    /* ISO C requires main to return int */
    return 0;
}
