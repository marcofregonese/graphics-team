PS-1 group Fregonese-Prinz-Recchia

REPORT OF FINAL ASSIGNMENT 5

Solar system description

Sun and planets from Mercury to Pluto with relative major satellites if any.
Every planet has its own orbit with relative inclination and its own revolution and rotation speed.
Satellites have different orbit inclinations as well.

Implementation 

The final assignment uses glm because it simplifies the implementation and the code looks cleaner.

Implementation of solar system

All solar system elements use the same uvsphere.obj file, see readMesh() in Initialize() function. 
Every planets has its own texture, see Display() function.
For the solar system animation every element is associated to its own chain of transformation matrices, see OnIdle() function.

Camera

Every planet hat its own camera following the planet while being centered on the sun. The keys for the cameras are '2', starting from Mercury, and in order of distance from the sun, ending with '0' Pluto.
Every camera can be shifted along the Y-axis using 'C' up or 'c' down and the Z-axis using '+' or '-'.
For the interactive user input see Keyboard() function.

Lighting

There are two light sources: the brightest from the sun and another with a less bright bluish color from outside the solar system.
The Phong lighting is implemented and with keys 'a', 's' and 'd' the user can increase rispectively ambient, specular and diffuse light effects.The Blinn-Phong shading is implemented as well, see vertex and fragment shaders.

Extra effects

We implemented the skydome to get a "space" effect with the stars all around the solar system.
We added a quad with createQuadMesh(), we put the saturn's ring texture on it and then we modified the alpha value of the output color in the fragment shader in order to get the transparency effect.

Extra notes

inside example_glm/ folder compile with: 
$ make 
and execute with:
$ MESA_GL_VERSION_OVERRIDE=3.3 ./example_glm
We set winWidth = 1400.0f; variable in Initialize() function so that you can use the full screen to view the project keeping the right dimensions.
You can have extra informations about the single planets, satellites, the whole project and its implementation reading the comments along the code in example_glm.cpp.
