# Graphics 2018: "Solar system" project

Languages: C/C++

## assignment 1

In this cube solar system there are 9 cubes:
1. Sun
2. Coplanar orbit planet rotating around its own axis
3. Coplanar orbit planet rotating around its own axis and 23 degrees inclined + relative satellite
4. No-coplanar orbit planet rotating around its on axis
5. No-coplanar orbit planet rotating around its own axis + relative satellite
6. Co-planar elliptic translated orbit planet rotating around its own axis

The planets rotates around their own axes and around the sun with different speeds.

## assignment 2

1. nine .obj files called in Initialize() by readMeshFile()
2. same orbits as in assignemt 1, see OnIdle()

## assignment 3

1. Blinn Phong shading done
2. Solar system added
3. Moved first light source at sun position and added second light source
4. Added ambient, diffuse and specular parameter keyborad controls('a'/'d'/'s')

## assignment 4

1. Added planets and texture
2. Adjusted vertex and fragment shaders according to assignment 3

## final assignment-presentation

1. Migrated all code to c++ in order to use glm functions that simplify the code
2. Added camera following single planets
3. all planets done from mercury to plato with realistic dimensions
4. added satellites of Jupiter, Saturn, Uranus and Neptune
5. added zoomin '+',  zoomout '-' and moving camera on Y axis up:'C' and down:'c'
6. added skydome effect with stars on background
