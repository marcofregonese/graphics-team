#version 330

in vec3 normal;
in vec3 color;
in vec3 position;
in mat4 modelViewMatrix;
in mat4 normalMatrix;

in vec2 UVcoords;
in vec4 vColor;

uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;

uniform vec3 LightColor1;
uniform vec3 LightPosition1;
uniform vec3 LightColor2;
uniform vec3 LightPosition2;

uniform float DiffuseFactor;
uniform float SpecularFactor;
uniform float AmbientFactor;

uniform sampler2D myTextureSampler;
uniform int UseTexture;

out vec4 FragColor;

void main()
{
    vec3 nrml = normalize((normalMatrix * vec4(normalize(normal), 1.0)).xyz);
    vec3 lightPosition1 = (ViewMatrix * vec4(LightPosition1, 1.0)).xyz;
    vec3 lightPosition2 = (ViewMatrix * vec4(LightPosition2, 1.0)).xyz;

    // Compute vertex position in Model space
    vec4 vertexPositionModelSpace = modelViewMatrix * vec4(position,1.0);

    // vertex to eye vector (V)
    vec3 vertexNormalized = normalize(-vertexPositionModelSpace.xyz);

    // vertex to lightsource vector (L)
    vec3 lightVector1 = normalize(lightPosition1 - vertexPositionModelSpace.xyz);
    vec3 lightVector2 = normalize(lightPosition2 - vertexPositionModelSpace.xyz);
    
    // half vector (H = V + L)
    vec3 halfVector1 = normalize(lightVector1 + vertexNormalized);
    vec3 halfVector2 = normalize(lightVector2 + vertexNormalized);

    // SHADING 

    // Ambient term 
    vec3 ambientPart = vec3(color * AmbientFactor);

    // Diffuse term
    vec3 diffusePart1 = clamp(dot(nrml, lightVector1), 0.0, 1.0)*LightColor1;
    diffusePart1 *= vec3(DiffuseFactor);
    vec3 diffusePart2 = clamp(dot(nrml, lightVector2), 0.0, 1.0)*LightColor2;
    diffusePart2 *= vec3(DiffuseFactor);
    vec3 diffusePart = diffusePart1 + diffusePart2;

    // Specular term
    vec3 specularPart1 = pow(clamp(dot(nrml, halfVector1),0.0,1.0),5.0 )*LightColor1;
    specularPart1 *= vec3(SpecularFactor);
    vec3 specularPart2 = pow(clamp(dot(nrml, halfVector2),0.0,1.0),5.0 )*LightColor2;
    specularPart2 *= vec3(SpecularFactor);
    vec3 specularPart = specularPart1 + specularPart2;

    // final color is the sum of 3 terms
    vec4 fc = vec4(color*diffusePart + specularPart + ambientPart, 1.0);

    // Read color at UVcoords position in the texture
    vec4 TexColor = texture2D(myTextureSampler, UVcoords);

    if(UseTexture == 0)
    {
        // If texture mapping disabled, return objects's color
        FragColor = vColor;
    }
    else
    {
        // If texture mapping enabled, return texture color
        FragColor = TexColor + fc;
    }
}
