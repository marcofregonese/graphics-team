#version 330

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Color;
layout (location = 2) in vec3 Normal;
layout (location = 3) in vec2 UV;


// Output sent to the fragment Shader
out vec3 lightVector1;
out vec3 halfVector1;
out vec3 normal;
out vec3 color;
out vec3 position;
out mat4 modelViewMatrix;
out mat4 normalMatrix;

out vec2 UVcoords;
out vec4 vColor;

void main()
{
    color = Color;
    position = Position;
    normal = Normal;

    // VECTORS

    // Compute modelview matrix
    modelViewMatrix = ViewMatrix * ModelMatrix;
    mat4 modelViewProjectionMatrix = ProjectionMatrix * modelViewMatrix;
            
    // Compute a 4*4 normal matrix
    normalMatrix = transpose(inverse(modelViewMatrix));

   gl_Position = ProjectionMatrix*ViewMatrix*ModelMatrix*vec4(Position.x, Position.y, Position.z, 1.0);
   
   vColor = vec4(Color.rgb, 1.0);
   
   UVcoords = UV;
}
