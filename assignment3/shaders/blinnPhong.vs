#version 330

// Uniform input
uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;

// Content of the vertex data
layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Color;
layout (location = 2) in vec3 Normal;

// Output sent to the fragment Shader
out vec3 lightVector1;
out vec3 halfVector1;
out vec3 normal;
out vec3 color;
out vec3 position;
out mat4 modelViewMatrix;
out mat4 normalMatrix;

// Main function
void main()
{

    color = Color;
    position = Position;
    normal = Normal;

    // VECTORS

    // Compute modelview matrix
    modelViewMatrix = ViewMatrix * ModelMatrix;
    mat4 modelViewProjectionMatrix = ProjectionMatrix * modelViewMatrix;
            
    // Compute a 4*4 normal matrix
    normalMatrix = transpose(inverse(modelViewMatrix));

    gl_Position = modelViewProjectionMatrix * vec4(Position, 1.0);

}
