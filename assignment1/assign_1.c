/******************************************************************
*
* Interaction.c  
*
* Description: This file demonstrates the loading of external 
* triangle meshes provided in OBJ format. In addition, user
* interaction via mouse and keyboard is employed.
*
* The loaded triangle mesh is draw in wireframe mode and rotated
* around a reference axis. The user can control the rotation axis
* via the mouse and start/stop/reset animation via the keyboard.
* 
* Interactive Graphics and Simulation Group
* Institute of Computer Science
* University of Innsbruck
*
*******************************************************************/


/* Standard includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* OpenGL includes */
#include <GL/glew.h>
#include <GL/freeglut.h>

/* Local includes */
#include "LoadShader.h"    /* Loading function for shader code */
#include "Matrix.h"        /* Functions for matrix handling */
#include "Vector.h"        /* Functions for vector handling */
#include "OBJParser.h"     /* Loading function for triangle meshes in OBJ format */


/*----------------------------------------------------------------*/
/* Window parameters */
float winWidth = 1000.0f;
float winHeight = 800.0f;

/* Flag for starting/stopping animation */
GLboolean anim = GL_TRUE;

/* Define handles to several vertex buffer objects */
GLuint VB0s[9];

/* Define handle to several color buffer objects */
GLuint CBOs[9];

/* Define handles to several index buffer objects */
GLuint IBOs[9];

/* Indices to vertex attributes; in this case positon only */ 
enum DataID {vPosition = 0, vColor = 1};

/* Strings for loading and storing shader code */
static const char* VertexShaderString;
static const char* FragmentShaderString;

GLuint ShaderProgram;

/* Number of objects in the scene */
int nbObjects = 9;

/* Matrices for uniform variables in vertex shader */
float ProjectionMatrix[16];     /* Perspective projection matrix */
float ViewMatrix[16];           /* Camera view matrix */
float ModelMatrix[9][16];       /* Array of model matrix */

  
/* Transformation matrices */
float RotationMatrixAnimX[16];
float RotationMatrixAnimY[16];
float RotationMatrixAnimZ[16];
float RotationMatrixAnim[16];

float Translate_2[16];
float Translate_4[16];
float Translate_6[16];
float Translate_8[16];
float Translate_10[16];
float Translate_12[16];
float Translate_14[16];
float Translate_08_00_12[16];
float Translate_m10_00_10[16];
float Translate_00_00_10[16];
float Translate_12_00_12[16];
float Translate_00_00_12[16];
float Translate_00_00_04[16];
float Translate_00_00_06[16];
float Translate_00_00_08[16];
float Translate_12_06_02[16];
float Translate_12_02_06[16];
float EmptyTranslateX[16];
float Scale_20[16];
float Scale_04[16];
float Scale_05[16];
float Scale_06[16];
float Scale_08[16];
float Scale_03[16];
float Scale_02[16];
float Scale_04_10_10[16];
float Scale_14_10_14[16];
float Scale_10_06_10[16];
float RotateX_10[16];
float RotateX_m10[16];
float RotateX_15[16];
float RotateX_20[16];
float RotateX_30[16];
float RotateX_23[16];
float RotateX_10[16];
float RotateY_30[16];
float RotateZ_30[16];

/* Variables for storing current rotation angles */
float angleX, angleY, angleZ = 0.0f; 
float angleX1 = 0.0f; 
float angleY1 = 0.0f; 
float angleY2 = 0.0f; 
float angleY3 = 0.0f; 
float angleY4 = 0.0f; 
float angleY5 = 0.0f; 
float angleY6 = 0.0f; 

float var = 0.0;

/* Indices to active rotation axes */
enum {Xaxis=0, Yaxis=1, Zaxis=2};
int axis = Yaxis;

/* Reference time for animation */
int oldTime = 0;

/* Counter */
int i;

/*----------------------------------------------------------------*/


/******************************************************************
*
* readMeshFile
*
* This function read the content of an OBJ file and then fill the
* buffer objects with the data
*
* Inpute: filename = name of file.obj
*         scale = uniform scale factor applied to the vertices
*         VBO = pointer to the Vertex buffer object to fill
*         CBO = pointer to the Color buffer object to fill
*         IBO = pointer to the Index buffer object to fill
*         rgb = 3D vector containing the color of the object (r=x, g=y, b=z)
*******************************************************************/

void readMeshFile(char* filename, float scale, GLuint* VBO, GLuint* CBO, GLuint* IBO, Vector rgb)
{
    /* Structure for loading of OBJ data */
    obj_scene_data data;

    /* Load first OBJ model */
    int success = parse_obj_scene(&data, filename);

    if(!success)
        printf("Could not load file. Exiting.\n");

    /*  Copy mesh data from structs into appropriate arrays */
    int vert = data.vertex_count;
    int indx = data.face_count;

    GLfloat* vertex_buffer_data = (GLfloat*) calloc (vert*3, sizeof(GLfloat));
    GLfloat* color_buffer_data = (GLfloat*) calloc (vert*3, sizeof(GLfloat));
    GLushort* index_buffer_data = (GLushort*) calloc (indx*3, sizeof(GLushort));

    /* Vertices */
    for(i=0; i<vert; i++)
    {
        vertex_buffer_data[i*3] = (GLfloat)(*data.vertex_list[i]).e[0]*scale;
        vertex_buffer_data[i*3+1] = (GLfloat)(*data.vertex_list[i]).e[1]*scale;
        vertex_buffer_data[i*3+2] = (GLfloat)(*data.vertex_list[i]).e[2]*scale;
    }

    /* Colors */
    for(i=0; i<vert; i++)
    {
        color_buffer_data[i*3] = (GLfloat)(rgb.x);
        color_buffer_data[i*3+1] = (GLfloat)(rgb.y);
        color_buffer_data[i*3+2] = (GLfloat)(rgb.z);
    }

    /* Indices */
    for(i=0; i<indx; i++)
    {
        index_buffer_data[i*3] = (GLushort)(*data.face_list[i]).vertex_index[0];
        index_buffer_data[i*3+1] = (GLushort)(*data.face_list[i]).vertex_index[1];
        index_buffer_data[i*3+2] = (GLushort)(*data.face_list[i]).vertex_index[2];
    }

    /* Create buffer objects and load data into buffers*/
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, *VBO);
    glBufferData(GL_ARRAY_BUFFER, data.vertex_count*3*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.face_count*3*sizeof(GLushort), index_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, CBO);
    glBindBuffer(GL_ARRAY_BUFFER, *CBO);
    glBufferData(GL_ARRAY_BUFFER, data.vertex_count*3*sizeof(GLfloat), color_buffer_data, GL_STATIC_DRAW);
}


/******************************************************************
*
* createCubeMesh
*
* This function creates a cube mesh and fill buffer objects with
* the geometry.
*
*******************************************************************/
void createCubeMesh(GLuint* VBO, GLuint* IBO, GLuint* CBO)
{
    GLfloat vertex_buffer_data[] = { /* 8 cube vertices XYZ */
        -1.0, -1.0,  1.0,
         1.0, -1.0,  1.0,
         1.0,  1.0,  1.0,
        -1.0,  1.0,  1.0,
        -1.0, -1.0, -1.0,
         1.0, -1.0, -1.0,
         1.0,  1.0, -1.0,
        -1.0,  1.0, -1.0,
    };

    GLfloat color_buffer_data[] = { /* RGB color values for 8 vertices */
        0.0, 0.0, 1.0,
        1.0, 0.0, 1.0,
        1.0, 1.0, 1.0,
        0.0, 1.0, 1.0,
        0.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
    };

    GLushort index_buffer_data[] = { /* Indices of 6*2 triangles (6 sides) */
        0, 1, 2,
        2, 3, 0,
        1, 5, 6,
        6, 2, 1,
        7, 6, 5,
        5, 4, 7,
        4, 0, 3,
        3, 7, 4,
        4, 5, 1,
        1, 0, 4,
        3, 2, 6,
        6, 7, 3,
    };

    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, *VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index_buffer_data), index_buffer_data, GL_STATIC_DRAW);

    glGenBuffers(1, CBO);
    glBindBuffer(GL_ARRAY_BUFFER, *CBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(color_buffer_data), color_buffer_data, GL_STATIC_DRAW);
}


/******************************************************************
*
* Display
*
* This function is called when the content of the window needs to be
* drawn/redrawn. It has been specified through 'glutDisplayFunc()';
* Enable vertex attributes, create binding between C program and 
* attribute name in shader, provide data for uniform variables
*
*******************************************************************/

void Display()
{
    /* Clear window; color specified in 'Initialize()' */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* For each object in the scene */
    for(i = 0; i < nbObjects; i++)
    {
        /* Bind buffer with vertex data of currently active object */
        glEnableVertexAttribArray(vPosition);
        glBindBuffer(GL_ARRAY_BUFFER, VB0s[i]);
        glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

        /* Bind color buffer */
        glEnableVertexAttribArray(vColor);
        glBindBuffer(GL_ARRAY_BUFFER, CBOs[i]);
        glVertexAttribPointer(vColor, 3, GL_FLOAT,GL_FALSE, 0, 0);

        /* Bind index buffer */
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBOs[i]);

        GLint size;
        glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);

        /* Associate program with uniform shader matrices */
        GLint projectionUniform = glGetUniformLocation(ShaderProgram, "ProjectionMatrix");
        if (projectionUniform == -1)
        {
            fprintf(stderr, "Could not bind uniform ProjectionMatrix\n");
            exit(-1);
        }
        glUniformMatrix4fv(projectionUniform, 1, GL_TRUE, ProjectionMatrix);

        GLint ViewUniform = glGetUniformLocation(ShaderProgram, "ViewMatrix");
        if (ViewUniform == -1)
        {
            fprintf(stderr, "Could not bind uniform ViewMatrix\n");
            exit(-1);
        }
        glUniformMatrix4fv(ViewUniform, 1, GL_TRUE, ViewMatrix);

        GLint RotationUniform = glGetUniformLocation(ShaderProgram, "ModelMatrix");
        if (RotationUniform == -1)
        {
            fprintf(stderr, "Could not bind uniform ModelMatrix\n");
            exit(-1);
        }
        glUniformMatrix4fv(RotationUniform, 1, GL_TRUE, ModelMatrix[i]);


        /* Issue draw command, using indexed triangle list */
        glDrawElements(GL_TRIANGLES, size/sizeof(GLushort), GL_UNSIGNED_SHORT, 0);

        /* Disable attributes */
        glDisableVertexAttribArray(vPosition);
        glDisableVertexAttribArray(vColor);
    }
    /* Swap between front and back buffer */
    glutSwapBuffers();
}


/******************************************************************
*
* Mouse
*
* Function is called on mouse button press; has been seta
* with glutMouseFunc(), x and y specify mouse coordinates,
* but are not used here.
*
*******************************************************************/

void Mouse(int button, int state, int x, int y)
{
    if(state == GLUT_DOWN)
    {
        printf("mouse pressed \n");
    }

    glutPostRedisplay();
}


/******************************************************************
*
* Keyboard
*
* Function to be called on key press in window; set by
* glutKeyboardFunc(); x and y specify mouse position on keypress;
* not used in this example 
*
*******************************************************************/

void Keyboard(unsigned char key, int x, int y)
{
    /* Here we change the rotation axis when a key is pressed */
    switch( key )
    {
        case 'x': case 'X':
            axis = Xaxis;
            SetIdentityMatrix(RotationMatrixAnimY);
            SetIdentityMatrix(RotationMatrixAnimZ);
            break;

        case 'y': case 'Y':
            axis = Yaxis;
            SetIdentityMatrix(RotationMatrixAnimX);
            SetIdentityMatrix(RotationMatrixAnimZ);
            break;

        case 'z': case 'Z':
            axis = Zaxis;
            SetIdentityMatrix(RotationMatrixAnimX);
            SetIdentityMatrix(RotationMatrixAnimY);
            break;

        case 'q': case 'Q':
            exit(0);
                break;
    }

    glutPostRedisplay();
}


/******************************************************************
*
* OnIdle
*
* Function executed when no other events are processed; set by
* call to glutIdleFunc(); holds code for animation  
*
*******************************************************************/

void OnIdle()
{
    /* Determine delta time between two frames to ensure constant animation */
    int newTime = glutGet(GLUT_ELAPSED_TIME);
    int delta = newTime - oldTime;
    oldTime = newTime;

    float EmptyMatrix0[16];
    float EmptyMatrix1[16];
    float EmptyMatrix2[16];
    float EmptyMatrix3[16];
    float EmptyMatrix4[16];
    float EmptyMatrix5[16];
    float EmptyMatrix6[16];
    float EmptyMatrix7[16];
    float EllipticMatrix[16];
    
    float IDMatrix[16];
    SetIdentityMatrix(IDMatrix);

    float RotationMatrixAnimS1[16];
    float RotationMatrixAnimY1[16];
    float RotationMatrixAnimNC[16];
    float RotationMatrixAnimY2[16];
    float RotationMatrixAnimP1[16];
    float RotationMatrixAnimE[16];
    float RotationMatrixAnimY3[16];
    float RotationMatrixAnimY4[16];
    float RotationMatrixAnimP2[16];
    float RotationMatrixAnimNC2[16];
    float RotationMatrixAnimS2[16];
    float RotationMatrixAnimY5[16];
    float RotationMatrixAnimP6[16];
    float RotationMatrixAnimY6[16];


    if(anim)
    {
        /* Increment rotation angle and update matrix */
        angleY = fmod(angleY + delta/120.0, 360.0);
        SetRotationY(angleY, RotationMatrixAnimY);

        angleY1 = fmod(angleY1 + delta/180.0, 360.0);
        SetRotationY(angleY1, RotationMatrixAnimY1);

        angleY2 = fmod(angleY2 + delta/70.0, 360.0);
        SetRotationY(angleY2, RotationMatrixAnimY2);

        angleY3 = fmod(angleY3 + delta/100.0, 360.0);
        SetRotationY(angleY3, RotationMatrixAnimY3);
        if(angleY3<180){ 
            var = var + 0.01; 
            SetTranslation(var, 0, 0, EmptyTranslateX);
            MultiplyMatrix(RotationMatrixAnimY3, EmptyTranslateX, EllipticMatrix);
        }
        else{
            var = var - 0.01;
            SetTranslation(var, 0, 0, EmptyTranslateX);
            MultiplyMatrix(RotationMatrixAnimY3, EmptyTranslateX, EllipticMatrix);
        }

        angleY4 = fmod(angleY4 + delta/50.0, 360.0);
        SetRotationY(angleY4, RotationMatrixAnimY4);

        angleY5 = fmod(angleY5 + delta/10.0, 360.0);
        SetRotationY(angleY5, RotationMatrixAnimY5);

        angleY6 = fmod(angleY6 + delta/160.0, 360.0);
        SetRotationY(angleY6, RotationMatrixAnimY6);
    }


    /* Update of transformation matrices
     * Note order of transformations and rotation of reference axes */
    MultiplyMatrix(RotationMatrixAnimX, RotationMatrixAnimY, RotationMatrixAnim);
    MultiplyMatrix(RotationMatrixAnim, RotationMatrixAnimZ, RotationMatrixAnim);

    /* Model matrix of the first object(sun) */
    MultiplyMatrix(RotationMatrixAnim, Scale_06, ModelMatrix[0]);    

    //matrix for planet coplanar orbit rotation
    MultiplyMatrix(RotationMatrixAnimX, RotationMatrixAnimY, RotationMatrixAnimP1);
    MultiplyMatrix(RotationMatrixAnimP1, RotationMatrixAnimZ, RotationMatrixAnimP1);

    /* Model matrix of the second object(planet1) */
    MultiplyMatrix(RotationMatrixAnimP1, Translate_8, EmptyMatrix0);
    MultiplyMatrix(EmptyMatrix0, RotateX_23, EmptyMatrix0);
    MultiplyMatrix(EmptyMatrix0, RotationMatrixAnimY4, EmptyMatrix0);
    MultiplyMatrix(EmptyMatrix0, Scale_04, ModelMatrix[1]);

    //matrix for satellite1 of planet1 coplanar orbit rotation
    MultiplyMatrix(RotationMatrixAnimX, RotationMatrixAnimY, RotationMatrixAnimS1);
    MultiplyMatrix(RotationMatrixAnimS1, RotationMatrixAnimZ, RotationMatrixAnimS1);
    MultiplyMatrix(RotationMatrixAnimS1, RotationMatrixAnimY1, RotationMatrixAnimS1);

    /* Model matrix of the third object(satellite1) */
    MultiplyMatrix(RotationMatrixAnim, Translate_8, EmptyMatrix1);
    MultiplyMatrix(EmptyMatrix1, Scale_02, EmptyMatrix1);
    MultiplyMatrix(EmptyMatrix1, RotationMatrixAnimS1, EmptyMatrix1);
    MultiplyMatrix(EmptyMatrix1, Translate_12, ModelMatrix[2]);

    //matrix for no-coplanar orbit
    MultiplyMatrix(RotationMatrixAnimX, RotateX_15, RotationMatrixAnimNC);
    MultiplyMatrix(RotationMatrixAnimNC, RotationMatrixAnimY, RotationMatrixAnimNC);
    MultiplyMatrix(RotationMatrixAnimNC, RotationMatrixAnimZ, RotationMatrixAnimNC);

    /* Model matrix of the fourth object (planet on no-coplanar orbit) */
    MultiplyMatrix(RotationMatrixAnimNC, Translate_m10_00_10, EmptyMatrix2);
    MultiplyMatrix(EmptyMatrix2, RotationMatrixAnimY3, EmptyMatrix2);
    MultiplyMatrix(EmptyMatrix2, Scale_02, ModelMatrix[3]);

    //matrix for coplanar elliptic translated orbit
    MultiplyMatrix(RotationMatrixAnimX, RotationMatrixAnimZ, RotationMatrixAnimE);
    MultiplyMatrix(RotationMatrixAnimE, EllipticMatrix, RotationMatrixAnimE);
    MultiplyMatrix(RotationMatrixAnimE, Translate_4, RotationMatrixAnimE);

    /* Model matrix of the fifth object (planet on elliptic orbit) */
    MultiplyMatrix(RotationMatrixAnimE, Translate_00_00_12, EmptyMatrix3);
    MultiplyMatrix(EmptyMatrix3, RotationMatrixAnimY3, EmptyMatrix3);
    MultiplyMatrix(EmptyMatrix3, Scale_02, ModelMatrix[4]);

    //matrix for planet coplanar orbit rotation
    MultiplyMatrix(RotationMatrixAnimX, RotationMatrixAnimY2, RotationMatrixAnimP2);
    MultiplyMatrix(RotationMatrixAnimP2, RotationMatrixAnimZ, RotationMatrixAnimP2);

    /* Model matrix of the sixth object (planet on normal orbit) */
    MultiplyMatrix(RotationMatrixAnimP2, Translate_00_00_04, EmptyMatrix4);
    MultiplyMatrix(EmptyMatrix4, RotationMatrixAnimY5, EmptyMatrix4);
    MultiplyMatrix(EmptyMatrix4, Scale_02, ModelMatrix[5]);

    //matrix for no-coplanar orbit
    MultiplyMatrix(RotationMatrixAnimX, RotateX_m10, RotationMatrixAnimNC2);
    MultiplyMatrix(RotationMatrixAnimNC2, RotationMatrixAnimY1, RotationMatrixAnimNC2);
    MultiplyMatrix(RotationMatrixAnimNC2, RotationMatrixAnimZ, RotationMatrixAnimNC2);

    /* Model matrix of the seventh object(planet3) */
    MultiplyMatrix(RotationMatrixAnimNC2, Translate_12_00_12, EmptyMatrix5);
    MultiplyMatrix(EmptyMatrix5, RotateX_10, EmptyMatrix5);
    MultiplyMatrix(EmptyMatrix5, RotationMatrixAnimY3, EmptyMatrix5);
    MultiplyMatrix(EmptyMatrix5, Scale_03, ModelMatrix[6]);

    //matrix for satelliter2 of planet1 coplanar orbit rotation
    MultiplyMatrix(RotationMatrixAnimX, RotateX_m10, RotationMatrixAnimS2);
    MultiplyMatrix(RotationMatrixAnimS2, RotationMatrixAnimY1, RotationMatrixAnimS2);
    MultiplyMatrix(RotationMatrixAnimS2, RotationMatrixAnimZ, RotationMatrixAnimS2);
    MultiplyMatrix(RotationMatrixAnimS2, RotationMatrixAnimY2, RotationMatrixAnimS2);

    // Model matrix of the eighth object (satellite2)
    MultiplyMatrix(RotationMatrixAnimNC2, Translate_12_00_12, EmptyMatrix6);
    MultiplyMatrix(EmptyMatrix6, Scale_02, EmptyMatrix6);
    MultiplyMatrix(EmptyMatrix6, RotationMatrixAnimS2, EmptyMatrix6);
    MultiplyMatrix(EmptyMatrix6, Translate_12, ModelMatrix[7]);

    //matrix for nineth planet rotation
    MultiplyMatrix(RotationMatrixAnim, RotationMatrixAnimY6, RotationMatrixAnimP6);
    MultiplyMatrix(RotationMatrixAnimP6, RotationMatrixAnimZ, RotationMatrixAnimP6);

    /* Model matrix of the nineth object(planet xxx) */
    MultiplyMatrix(RotationMatrixAnimP6, Translate_12_00_12, EmptyMatrix7);
    MultiplyMatrix(EmptyMatrix7, RotateX_10, EmptyMatrix7);
    MultiplyMatrix(EmptyMatrix7, RotationMatrixAnimY3, EmptyMatrix7);
    MultiplyMatrix(EmptyMatrix7, Scale_03, ModelMatrix[8]);

    /* Issue display refresh */
    glutPostRedisplay();
}



/******************************************************************
*
* AddShader
*
* This function creates and adds individual shaders
*
*******************************************************************/

void AddShader(GLuint ShaderProgram, const char* ShaderCode, GLenum ShaderType)
{
    /* Create shader object */
    GLuint ShaderObj = glCreateShader(ShaderType);

    if (ShaderObj == 0) 
    {
        fprintf(stderr, "Error creating shader type %d\n", ShaderType);
        exit(0);
    }

    /* Associate shader source code string with shader object */
    glShaderSource(ShaderObj, 1, &ShaderCode, NULL);

    GLint success = 0;
    GLchar InfoLog[1024];

    /* Compile shader source code */
    glCompileShader(ShaderObj);
    glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);

    if (!success) 
    {
        glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
        fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
        exit(1);
    }

    /* Associate shader with shader program */
    glAttachShader(ShaderProgram, ShaderObj);
}


/******************************************************************
*
* CreateShaderProgram
*
* This function creates the shader program; vertex and fragment
* shaders are loaded and linked into program; final shader program
* is put into the rendering pipeline 
*
*******************************************************************/

void CreateShaderProgram()
{
    /* Allocate shader object */
    ShaderProgram = glCreateProgram();

    if (ShaderProgram == 0) 
    {
        fprintf(stderr, "Error creating shader program\n");
        exit(1);
    }

    /* Load shader code from file */
    VertexShaderString = LoadShader("shaders/vertexshader.vs");
    FragmentShaderString = LoadShader("shaders/fragmentshader.fs");

    /* Separately add vertex and fragment shader to program */
    AddShader(ShaderProgram, VertexShaderString, GL_VERTEX_SHADER);
    AddShader(ShaderProgram, FragmentShaderString, GL_FRAGMENT_SHADER);

    GLint Success = 0;
    GLchar ErrorLog[1024];

    /* Link shader code into executable shader program */
    glLinkProgram(ShaderProgram);

    /* Check results of linking step */
    glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

    if (Success == 0) 
    {
        glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    /* Check if shader program can be executed */ 
    glValidateProgram(ShaderProgram);
    glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);

    if (!Success) 
    {
        glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    /* Put linked shader program into drawing pipeline */
    glUseProgram(ShaderProgram);
}


/******************************************************************
*
* Initialize
*
* This function is called to initialize rendering elements, setup
* vertex buffer objects, and to setup the vertex and fragment shader;
* meshes are loaded from files in OBJ format; data is copied from
* structures into vertex and index arrays
*
*******************************************************************/

void Initialize()
{   

    /* Create 2 cube meshes */
    Vector color;
    color.x = 1.0; color.y = 0.5; color.z = 0.0; //orange
    char* filename = "models/sphere.obj";
    readMeshFile(filename, 1.0f, &VB0s[0], &CBOs[0], &IBOs[0], color); //Sun
    
    color.x = 0.5; color.y = 0.0; color.z = 1.0; //lila
    readMeshFile(filename, 1.0f, &VB0s[1], &CBOs[1], &IBOs[1], color); //Planet1

    color.x = 0.0; color.y = 1.0; color.z = 1.0; //cyan
    filename = "models/satellite.obj";
    readMeshFile(filename, 0.3f, &VB0s[2], &CBOs[2], &IBOs[2], color); //Satellite to Planet 1

    color.x = 1.0; color.y = 0.0; color.z = 1.0; //pink
    filename = "models/sphere.obj";
    readMeshFile(filename, 1.0f, &VB0s[3], &CBOs[3], &IBOs[3], color); //Planet 2 on ... orbit

    color.x = 1.0; color.y = 0.0; color.z = 0.0; //red
    readMeshFile(filename, 1.0f, &VB0s[4], &CBOs[4], &IBOs[4], color); //Planet 3 on ... orbit

    color.x = 0.0; color.y = 1.0; color.z = 0.0; //green
    readMeshFile(filename, 1.0f, &VB0s[5], &CBOs[5], &IBOs[5], color); //Planet4 

    color.x = 0.0; color.y = 0.0; color.z = 1.0; //blue
    readMeshFile(filename, 1.0f, &VB0s[6], &CBOs[6], &IBOs[6], color); //Planet5 

    color.x = 1.0; color.y = 1.0; color.z = 1.0; //white
    filename = "models/satellite.obj";
    readMeshFile(filename, 0.2f, &VB0s[7], &CBOs[7], &IBOs[7], color); //Sattelite for Planet 5 

    color.x = 1.0; color.y = 1.0; color.z = 0.0; //yellow
    filename = "models/sphere.obj";
    readMeshFile(filename, 0.8f, &VB0s[8], &CBOs[8], &IBOs[8], color); //Planet 6 on ... orbit

	
    /* Set background (clear) color to blue */ 
    glClearColor(0.0, 0.0, 0.0, 0.0);

    /* Enable depth testing */
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);    


    /* Setup shaders and shader program */
    CreateShaderProgram();  


    /* Initialize matrices */
    SetIdentityMatrix(ProjectionMatrix);
    SetIdentityMatrix(ViewMatrix);
    SetScaleMatrix(2.0f,2.0f,2.0f,Scale_20); 
    SetScaleMatrix(0.4f,0.4f,0.4f,Scale_04); 
    SetScaleMatrix(0.5f,0.5f,0.5f,Scale_05); 
    SetScaleMatrix(0.6f,0.6f,0.6f,Scale_06); 
    SetScaleMatrix(0.8f,0.8f,0.8f,Scale_08); 
    SetScaleMatrix(0.2f,0.2f,0.2f,Scale_02); 
    SetScaleMatrix(0.3f,0.3f,0.3f,Scale_03); 
    SetScaleMatrix(0.4f,1.0f,1.0f,Scale_04_10_10); 
    SetScaleMatrix(1.4f,1.0f,1.4f,Scale_14_10_14); 
    SetScaleMatrix(1.0f,0.6f,1.0f,Scale_10_06_10); 

    /* Initalize model matrices */
    for(i=0; i< nbObjects; i++)
        SetIdentityMatrix(ModelMatrix[i]);

    SetTranslation(2, 0, 0, Translate_2);
    SetTranslation(4, 0, 0, Translate_4);
    SetTranslation(6, 0, 0, Translate_6);
    SetTranslation(8, 0, 0, Translate_8);
    SetTranslation(10, 0, 0, Translate_10);
    SetTranslation(12, 0, 0, Translate_12);
    SetTranslation(14, 0, 0, Translate_14);
    SetTranslation(8, 0, 12, Translate_08_00_12);
    SetTranslation(-10, 0, 10, Translate_m10_00_10);
    SetTranslation(12, 0, 12, Translate_12_00_12);
    SetTranslation(0, 0, 4, Translate_00_00_04);
    SetTranslation(0, 0, 6, Translate_00_00_06);
    SetTranslation(0, 0, 8, Translate_00_00_08);
    SetTranslation(0, 0, 12, Translate_00_00_12);
    SetTranslation(0, 0, 10, Translate_00_00_10);
    SetTranslation(12, 6, 2, Translate_12_06_02);
    SetTranslation(12, 2, 6, Translate_12_02_06);

    SetRotationX(30, RotateX_30);
    SetRotationX(10, RotateX_10);
    SetRotationX(-10, RotateX_m10);
    SetRotationX(15, RotateX_15);
    SetRotationX(20, RotateX_20);
    SetRotationX(23, RotateX_23);
    SetRotationY(30, RotateY_30);
    SetRotationZ(30, RotateZ_30);

    /* Initialize animation matrices */
    SetIdentityMatrix(RotationMatrixAnimX);
    SetIdentityMatrix(RotationMatrixAnimY);
    SetIdentityMatrix(RotationMatrixAnimZ);
    SetIdentityMatrix(RotationMatrixAnim);
    
    /* Set projection transform */
    winWidth = 1000.0f;
    winHeight = 800.0f;
    float aspect = winWidth / winHeight;
    float nearPlane = 1.0; 
    float farPlane = 50.0;
    SetPerspectiveMatrix(45.0, aspect, nearPlane, farPlane, ProjectionMatrix);

    /* Set viewing transform */
    float RotationMatrix[16];
    SetRotationX(15.0, RotationMatrix);
    float camera_disp = -20.0;
    SetTranslation(0.0, -5.0, camera_disp, ViewMatrix);
    MultiplyMatrix(RotationMatrix, ViewMatrix, ViewMatrix);

}


/******************************************************************
*
* main
*
* Main function to setup GLUT, GLEW, and enter rendering loop
*
*******************************************************************/

int main(int argc, char** argv)
{
    /* Initialize GLUT; set double buffered window and RGBA color model */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(winWidth, winHeight);
    glutInitWindowPosition(200, 100);
    glutCreateWindow("CG Proseminar - Transformations");

    /* Initialize GL extension wrangler */
    GLenum res = glewInit();
    if (res != GLEW_OK) 
    {
        fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
        return 1;
    }

    /* Setup scene and rendering parameters */
    Initialize();

    /* Specify callback functions;enter GLUT event processing loop, 
     * handing control over to GLUT */
    glutIdleFunc(OnIdle);
    glutDisplayFunc(Display);
    glutKeyboardFunc(Keyboard); 
    glutMouseFunc(Mouse);

    glutMainLoop();

    /* ISO C requires main to return int */
    return 0;
}
